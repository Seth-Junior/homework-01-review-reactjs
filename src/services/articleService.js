import api from '../utils/api';

export const fetchArticle = async () => {
	let response = await api.get("/articles");
	return response.data.data;
};
export const fetchArticleByID = async (id) => {
	let response = await api.get(`/articles/${id}`);
	return response.data.data;
}
export const deleteArticleByID = async (id) => {
	let response = await api.delete(`/articles/${id}`);
	// console.log("Delete message",response);
	return response.data.message;
};
export const insertArticle = async (arti) => {
	let response = await api.post('/articles', arti);
	return response.data.data;
};
export const uploadImage = async (file) => {
	let formData = new FormData();
	formData.append('image', file);

	let response = await api.post('images', formData);
	return response.data.url;
};
export const updateArticleById = async (id, updateArticle) => {
	let response = await api.patch(`/articles/${id}`, updateArticle);
	return response.data.message;
}
