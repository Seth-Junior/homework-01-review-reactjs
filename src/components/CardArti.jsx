import React, { useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import './CardArt.css'
import { onDeleteArticleById } from '../redux/actions/articleAction';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import Swal from "sweetalert2/dist/sweetalert2.js";
import "sweetalert2/dist/sweetalert2.css";
import { onFetchArticle } from '../redux/actions/articleAction';


export default function CardArti({ article }) {

    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(onFetchArticle());
    }, []);

    const deleteArticle = bindActionCreators(onDeleteArticleById, dispatch)

    const onDelete = (id) => {
        Swal.fire({
            title: "Are you surre to delete ?",
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "Yes" ,
            cancelButtonText: "No",
        }).then((result) => {
            if (result.isConfirmed) {
                if (deleteArticle(id)) {
                    Swal.fire(
                        "Deleted",
                        "Article has been deleted ! ",
                        'success'
                    )
                }
            }
        })
    };

    return (
        <Card key={article._id}>
            <Card.Img variant="top" src={article.image} />
            <Card.Body>
                <Card.Title>{article.title}</Card.Title>
                <Card.Text>{article.description}</Card.Text>
                <>
                    <Button variant="warning" onClick={() => history.push(`/article/update/${article._id}`)}>Update</Button>{' '}
                    <Button variant="danger" onClick={() => onDelete(article._id)}>Delete</Button>{' '}
                    <Button variant="primary" onClick={() => history.push('/detail/' + article._id)} >More</Button>
                </>
            </Card.Body>
        </Card>
    )
}
