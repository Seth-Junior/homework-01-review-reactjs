import { articleActionType } from "../actions/actionType";

const initialState = {
	articles: [],
	article: [],
	isLoading: true,
};

const articleReducer = (state = initialState, { type, payload, isLoading }) => {
	switch (type) {
		case articleActionType.FETCH_ARTICLE:
			return {
				...state,
				articles: [payload],
				isLoading: isLoading
			};
		case articleActionType.FETCH_ARTICLE_BY_ID:
			return {
				...state,
				article: [payload],
				isLoading: isLoading
			};
		case articleActionType.DELETE_ARTICLE_BY_ID:
			return {
				...state,
				articles: [state.articles.filter((article) => article._id !== payload)]
			};
		case articleActionType.INSERT_ARTICLE:
			return {
				...state,
				articles: [payload, ...state.articles]
			};
		case articleActionType.UPDATE_ARTICLE:
			// console.log(action);
			let newArticles = [...state.articles];
			newArticles = newArticles.map((article) => {
				if (article._id === payload.articleId) {
					article.title = payload.updateArticle.title;
					article.description = payload.updateArticle.description;
					article.image = payload.updateArticle.image;
				}
				return article;
			});

			return { ...state, articles: newArticles };

		default:
			return state;
	}
};

export default articleReducer;
