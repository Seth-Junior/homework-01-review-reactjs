import { fetchArticle , fetchArticleByID , deleteArticleByID ,insertArticle ,updateArticleById } from "../../services/articleService";
import { articleActionType } from "./actionType";


export const onFetchArticle = ( ) => async (dispatch) => {
	let articles = await fetchArticle();
	dispatch({
		type: articleActionType.FETCH_ARTICLE,
		payload: articles,
		isLoading: false,
	})
}
export const onFetchArticleById = (id) => async (dispatch) => {
	let article = await fetchArticleByID(id);
	dispatch({
		type: articleActionType.FETCH_ARTICLE_BY_ID,
		payload: article,
		isLoading: false,
	})
}
export const onDeleteArticleById = (artId) => async (dispatch) => {
	let message = await deleteArticleByID(artId);
	dispatch({
		type: articleActionType.DELETE_ARTICLE_BY_ID,
		payload: artId,
	});
};
export const onInsertArticle = (arti) => async (dispatch) => {
	let article = await insertArticle(arti);

	dispatch({
		type: articleActionType.INSERT_ARTICLE,
		payload: article,
	});
};
export const onUpdateArticle = (articleId, updatedArticle) => async (dispatch) => {
	let message = await updateArticleById(articleId, updatedArticle);

	dispatch({
		type: articleActionType.UPDATE_ARTICLE,
		payload: { articleId, updatedArticle },
	});
};