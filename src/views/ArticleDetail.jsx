import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { onFetchArticleById } from '../redux/actions/articleAction';
import { useParams } from 'react-router-dom';
import { Col, Container, Row ,Spinner } from 'react-bootstrap';


export default function ArticleDetail() {

    let param = useParams();
    const dispatch = useDispatch();
    const { article, isLoading } = useSelector(state => state.articleReducer)

    useEffect(() => {
        dispatch(onFetchArticleById(param.id))
    }, [])
    console.log("article detail",article);
    return (
        <div>
            {!isLoading ?
                article.map((det) =>
                    <Container className="p-4">
                        <Row>
                            <Col md={7}>
                                <h4>{det.title}</h4>
                                <p>{det.description}</p>
                            </Col>
                            <Col md={5}>
                                <img width="100%" src={!det.image ? "https://designshack.net/wp-content/uploads/placeholder-image.png" : det.image} />
                            </Col>
                        </Row>
                    </Container>
                ) :
                <Container className="text-center mt-5">
                    <Spinner animation="border" role="status">
                        <span className="visually-hidden">Loading...</span>
                    </Spinner>
                </Container>
            }
        </div>
    )
}
