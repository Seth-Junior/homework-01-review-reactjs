import React, { useEffect } from 'react';
import { Container, Button, Row, Col ,Spinner } from 'react-bootstrap';
import CardArti from '../components/CardArti';
import { useDispatch, useSelector } from 'react-redux';
import { onFetchArticle } from '../redux/actions/articleAction';
import { Link } from 'react-router-dom';


function Home() {


  const dispatch = useDispatch();

  const { articles, isLoading } = useSelector((state) => state.articleReducer);
  
  useEffect(() => {
    dispatch(onFetchArticle());
  },[]);


  // console.log("articles",articles);
  return (
    <div>
      {!isLoading ?
        articles && articles.map((value) => {
          // console.log('article', value);
          return (
            <Container className="pb-5">
              <div className="d-flex my-5">
                <h3 className="me-4" style={{ textDecoration: 'underline gray 5px' }}>New Feed</h3>
                <Button variant="primary" as={Link} to="/article/add_article">Add Article</Button>
              </div>
              <Row className="g-3">
                {value && value.map((ar) =>
                  <Col md={3} key={ar._id} >
                    <CardArti article={ar} />
                  </Col>
                )
                }
              </Row>
            </Container>
          )
        }
        ) :
        <Container className="text-center mt-5">
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </Container>
      }
    </div>
  );
}

export default Home;
