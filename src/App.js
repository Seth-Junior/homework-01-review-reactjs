import React from 'react';
import NavBar from './components/NavBar';
import Home from './views/Home';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import ArticleDetail from './views/ArticleDetail';
import AddArticle from './views/AddArticle';

function App() {
  return (
    <Router>
        <NavBar/>
        <Switch>
          <Route path="/detail/:id">
            <ArticleDetail />
          </Route>
          <Route path={["/article/add_article","/article/update/:id"]}>
            <AddArticle/>
          </Route>
          <Route path="/">
            <Home/>
          </Route>
        </Switch>
    </Router>
  );
}

export default App;
